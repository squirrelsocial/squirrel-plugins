import { DemoSharedBase } from '../utils';
import { isIOS, Dialogs, Utils } from '@nativescript/core';
import { Contacts } from '@squirrel-social/contacts';

export class DemoSharedContacts extends DemoSharedBase {
	testIt() {
		Contacts.getContacts(['display_name', 'phone', 'email'])
		.then((data) => {
				// sort contacts alphabetically
				const contacts = data.sort((a, b) => a.display_name.localeCompare(b.display_name));
				console.dir(contacts);
		})
		.catch((error) => {
				console.error(error);
				if (isIOS) {
					Dialogs.confirm({
						title: 'Cannot Access Contacts',
						message: 'You can enable access to your contacts in Settings > Squirrel > Contacts',
						okButtonText: 'Open Settings',
						cancelButtonText: 'Cancel',
					}).then((ok) => {
						if (ok) {
							// Open settings for Squirrel
							Utils.openUrl(UIApplicationOpenSettingsURLString);
						} else {
							console.log('canceled open settings');
						}
					});
				} else {
					console.log('canceled open settings');
				}
		});
	}
}
