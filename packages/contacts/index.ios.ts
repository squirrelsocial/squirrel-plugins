import { ContactsCommon, Contact } from './common';

export class Contacts extends ContactsCommon {

  private static HOME_FAX = "_$!<HomeFAX>!$_";
  private static WORK_FAX = "_$!<WorkFAX>!$_";
  private static MAIN = "_$!<Main>!$_";

  public static handlePermission(): Promise<void> {
    return new Promise((resolve, reject) => {
      const status = CNContactStore.authorizationStatusForEntityType(CNEntityType.Contacts)

      if (status === CNAuthorizationStatus.Authorized) { resolve(); }

      else if (status === CNAuthorizationStatus.NotDetermined) {
        this.requestPermission()
        .then(() => { resolve(); })
        .catch((error) => { reject(error); });
      } else {
        reject(`Unable to obtain permission to read contacts: ${status}`);
      }
    });
  }

  private static requestPermission() {
    return new Promise((resolve, reject) => {
      const contactStore = CNContactStore.new();
      contactStore.requestAccessForEntityTypeCompletionHandler(CNEntityType.Contacts, () => {
        this.handlePermission()
        .then(() => { resolve(); })
        .catch((error) => { reject(error); });
      });
    });
  }

  /**
   * @param fields - desired fields to retrieve from phone storage backend
   */
  public static getContactsFromBackend (fields: string[]): Promise<Contact[]> {
    return new Promise((resolve, reject) => {
      try {
        let contacts: Contact[] = []; // array containing contacts object to return
        let columnsToFetch = this.getiOSQueryColumns(fields); // columns to return for each row

        /* getting all contacts */
        const store = new CNContactStore();
        const fetch = CNContactFetchRequest.alloc().initWithKeysToFetch(columnsToFetch);
        const nativeMutableArray = new NSMutableArray<CNContact>({ capacity: 10000 });

        let error: interop.Pointer;

        fetch.unifyResults = true;
        fetch.predicate = null;

        store.enumerateContactsWithFetchRequestErrorUsingBlock(fetch, error, (contact, stop) => {
          nativeMutableArray.addObject(contact);
          // transform raw data into desired data structure
          contacts.push(this.convertNativeCursorToContact(contact, fields));
        });

        resolve(contacts);
      } catch (error) {
        reject(error);
      }
    });
  }

  /**
   *
   * @param fields - An array of field names to fetch
   * @returns - An array with column names to fetch from iOS Contact backend
   */
  private static getiOSQueryColumns(fields: string[]) {
    let columnsToFetch = [];

    if (fields.indexOf('display_name') > -1) { columnsToFetch.push(CNContactGivenNameKey, CNContactFamilyNameKey); }
    if (fields.indexOf('phone') > -1) { columnsToFetch.push(CNContactPhoneNumbersKey); }
    if (fields.indexOf('email') > -1) { columnsToFetch.push(CNContactEmailAddressesKey); }

    // filter out any nulls & duplicates
    return columnsToFetch.filter((item, index, self) => { return (item != null && index == self.indexOf(item)) });
  }

  private static getiOSValue(key: string, contactData: CNContact) {
    return contactData.isKeyAvailable(key) ? contactData[key] : null;
  };

  private static getGenericLabel(nativeLabel: string): string {
    let genericLabel = nativeLabel;

    switch (nativeLabel) {
      case CNLabelHome:
        genericLabel = 'home';
        break;
      case CNLabelWork:
        genericLabel = 'work';
        break;
      case CNLabelOther:
        genericLabel = 'other';
        break;
    };

    return genericLabel;
  };

  private static getPhoneLabel(nativeLabel: string): string {
    let phoneLabel = this.getGenericLabel(nativeLabel);

    switch (nativeLabel) {
      case kABPersonPhoneMobileLabel:
        phoneLabel = "mobile";
        break;
      case this.HOME_FAX:
        phoneLabel = 'fax_home';
        break;
      case this.WORK_FAX:
        phoneLabel = 'fax_work';
        break;
      case kABPersonPhonePagerLabel:
        phoneLabel = 'pager';
        break;
      case this.MAIN:
        phoneLabel = 'main';
        break;
    };

    return phoneLabel;
  };

  /**
   * Takes a native iOS contact row and converts to json object for new contact
   * @param cursor - the cursor returned by iOS' data store
   * @param fields - those the user is interested in (skip irrelevant properties for speedup)
   */
  private static convertNativeCursorToContact(cursor: CNContact, fields: string[]) {
    let contact = new Contact();
    contact.contact_id = this.getiOSValue("identifier", cursor);

    /* display_name */
    if (fields.indexOf("display_name") > -1) {
      contact.display_name = `${this.getiOSValue("givenName", cursor).trim()} ${this.getiOSValue("familyName", cursor).trim()}`.trim();
    }

    /* phone */
    if (fields.indexOf("phone") > -1) {
      for (let i = 0; i < cursor.phoneNumbers.count; i++) {
        const phoneData = cursor.phoneNumbers[i];
        contact.phone.push({
          type: this.getPhoneLabel(phoneData.label),
          number: phoneData.value.stringValue
        });
      }
    }

    /* email */
    if (fields.indexOf("email") > -1) {
      for (let i = 0; i < cursor.emailAddresses.count; i++) {
        const emailData = cursor.emailAddresses[i];
        contact.email.push({
          type: this.getGenericLabel(emailData.label),
          address: emailData.value
        });
      }
    }

    return contact;
  };
}
