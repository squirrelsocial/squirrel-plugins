import { Observable } from '@nativescript/core';
import { Contacts } from './index';

export class ContactsCommon extends Observable {
  public static getContacts(fields: string[]): Promise<Contact[]> {
    return new Promise((resolve, reject) => {
      Contacts.handlePermission()
      .then(async () => {
        const contacts = await Contacts.getContactsFromBackend(fields);
        resolve(contacts);
      })
      .catch((error) => {
        reject(error);
      });
    });
  }
}

export class Contact {
  contact_id: string
  display_name: string
  phone: ContactPhone[]
  email: ContactEmail[]
  constructor(contact_id?: string, display_name?: string, phone: ContactPhone[] = [], email: ContactEmail[] = [])  {
    this.contact_id = contact_id;
    this.display_name = display_name;
    this.phone = phone;
    this.email = email;
  }
}

class ContactPhone {
  type: string
  number: string
}

class ContactEmail {
  type: string
  address: string
}

