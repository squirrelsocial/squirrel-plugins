import { ContactsCommon, Contact } from './common';

export declare class Contacts extends ContactsCommon {
  public static getContacts(fields: string[]): Promise<Contact[]>
  public static handlePermission(): Promise<void>
  public static getContactsFromBackend(fields: string[]): Promise<Contact[]>
}
