import { Application } from '@nativescript/core'
import { hasPermission, requestPermission } from 'nativescript-permissions';
import { ContactsCommon, Contact } from './common';

export class Contacts extends ContactsCommon {
  private static MIME_TYPES = {
    phone : "vnd.android.cursor.item/phone_v2",
    email : "vnd.android.cursor.item/email_v2",
  }
  /*
    mapping of Android data types (needed for reverse lookup of "dataN" fields)
  */
  private static DATA_TYPES = {
    phone: {
      number: android.provider.ContactsContract.CommonDataKinds.Phone.NUMBER,
    },
    email: {
      address: android.provider.ContactsContract.CommonDataKinds.Email.ADDRESS,
    },
  }


  public static handlePermission(): Promise<void> {
    return new Promise((resolve, reject) => {
      // check whether we already have permissions
      if (hasPermission(android.Manifest.permission.READ_CONTACTS)) {
        return resolve();
      }

      requestPermission(
        android.Manifest.permission.READ_CONTACTS,
        'This application requires read-only access to your contacts!'
      ).then(() => {
        resolve();
      }).catch((error) => {
        console.error(error);
        reject('Unable to obtain permission to read contacts!');
      });
    });
  }

  /**
   * @param fields - desired fields to retrieve from phone storage backend
   */
  public static getContactsFromBackend (fields: string[]): Promise<Contact[]> {
    return new Promise((resolve, reject) => {
      try {
        const content_uri = android.provider.ContactsContract.Data.CONTENT_URI;   // The content URI of the words table
        // The columns to return for each row
        const columnsToFetch = this.getAndroidQueryColumns(fields);
        const selectionClause = this.getSelectionClause(fields);
        // Either null, or an array of string args for selection clause
        const selectionArgs = this.getSelectionArgs(fields);
        // The sort order for the returned rows
        const sortOrder = null;

        /* load raw data from android backend */
        const cursor = Application.android.context.getContentResolver().query(content_uri, columnsToFetch, selectionClause, selectionArgs, sortOrder);

        /*
          transform raw data into desired data structure
          moveToNext() moves the row pointer to the next row in the cursor
          initial position is -1 and retrieving data at that position you will get an exception
        */

        let contacts: Contact[] = []; // array containing contacts object to return

        while (cursor.moveToNext()) {
          let contact_id = this.getColumnValue(cursor, columnsToFetch.indexOf("contact_id"));
          // see if contact_id already exists in contacts to pass existing object for appending
          let existingContactObj = undefined;
          let existingContactIndex = contacts.findIndex((item) => { return item.contact_id === contact_id });
          if (existingContactIndex > -1) {
            existingContactObj = contacts[existingContactIndex];
          }

          let contact = this.convertNativeCursorToContact(cursor, fields, columnsToFetch, existingContactObj);
          if (existingContactIndex > -1) {
            contacts[existingContactIndex] = contact;
          } else {
            contacts.push(contact);
          }
        }
        cursor.close();

        resolve(contacts);
      } catch (error) {
        reject(error);
      }
    });
  }

  /**
   * returns an string with the selection clause, e.g. '(mimetype=? OR mimetype=?) AND searchTerm="Jon"'
   * @param fields
   */
  private static getSelectionClause(fields: string[]) {
    const clauseArray = [];
    const fieldsArray = [];
    let clause = '';

    this.getAndroidMimeTypes(fields).forEach((mimetype) => {
      fieldsArray.push('mimetype=?','OR');
    });
    fieldsArray.pop();
    if (fieldsArray.length > 0) {
      clauseArray.push(`(${fieldsArray.join(' ')})`,'AND');
    }

    clauseArray.pop();
    if (clauseArray.length > 0) {
      clause = clauseArray.join(' ');
    }
    return clause;
  }

  /**
   * returns an array of strings with the arguments for the selection clause,
   * e.g. ['vnd.android.cursor.item/name','vnd.android.cursor.item/photo']
   * @param fields
   */
  private static getSelectionArgs(fields: string[]) {
    return this.getAndroidMimeTypes(fields);
  }

/**
 * returns an array with names of relevant metadata types to fetch from storage backend
 * @param fields
 */
  private static getAndroidMimeTypes(fields: string[]) {
    let datatypes = [];

    if (fields.indexOf("phone") > -1) { datatypes.push(this.MIME_TYPES.phone); }
    if (fields.indexOf("email") > -1) { datatypes.push(this.MIME_TYPES.email); }

    return datatypes.filter((value) => { return value != null; }); // filter out any nulls
  }

  /*
    returns value for column with index columnIndex
    helper function for convertNativeCursorToContact
  */
  private static getColumnValue(cursor, columnIndex) {
    const columnType = cursor.getType(columnIndex);
    switch (columnType) {
      case 0: // NULL
        return null;
      case 1: // Integer
        return cursor.getInt(columnIndex);
      case 2: // Float
        return cursor.getFloat(columnIndex);
      case 3: // String
        return cursor.getString(columnIndex);
      case 4: // Blob
        return cursor.getBlob(columnIndex);
      default:
        throw new Error('Contact - Unknown column type '+ columnType);
    }
  }

  /**
   *
   * @param fields - An array of field names to fetch
   * @returns - An array with column names to fetch from iOS Contact backend
   */
  private static getAndroidQueryColumns(fields: string[]) {
    let columnsToFetch = [];

    columnsToFetch.push("contact_id", "mimetype");

    if (fields.indexOf("display_name") > -1) {
      columnsToFetch.push(android.provider.ContactsContract.ContactNameColumns.DISPLAY_NAME_PRIMARY);
    }
    if (fields.indexOf("phone") > -1) {
      columnsToFetch.push(android.provider.ContactsContract.CommonDataKinds.Phone.NUMBER);
    }
    if (fields.indexOf("email") > -1) {
      columnsToFetch.push(android.provider.ContactsContract.CommonDataKinds.Email.DISPLAY_NAME);
    }

    // filter out any nulls & duplicates
    return columnsToFetch.filter((item, index, self) => { return (item != null && index == self.indexOf(item)) });
  }

  /**
   * Takes a native android contact row and converts to json object for new contact
   * If contact already exists then merge relevant properties with the existing object
   * @param cursor - the cursor returned by iOS' data store
   * @param fields - those the user is interested in (skip irrelevant properties for speedup)
   * @param columnNames - since we already have those, no need to check them for every iteration
   * @param existingContact - either undefined or object for an existing record
   */
  private static convertNativeCursorToContact(cursor, fields: string[], columnNames: string[], existingContact) {
    let contact = new Contact(); // contact object to return if creating a new record
    if (existingContact) { contact = existingContact; }

    if (!existingContact) {
      contact.contact_id = this.getColumnValue(cursor, columnNames.indexOf("contact_id"));
    }

    /* display_name */
    if (fields.indexOf("display_name") > -1 && !existingContact) {
      contact.display_name = this.getColumnValue(cursor, columnNames.indexOf("display_name"));
    }

    /* phone */
    if (fields.indexOf("phone") > -1 && this.getColumnValue(cursor, columnNames.indexOf("mimetype")) === this.MIME_TYPES.phone) {
      let number = this.getColumnValue(cursor, columnNames.indexOf(this.DATA_TYPES.phone.number));
      contact.phone.push({
        type: null,
        number: number ? number.trim() : null,
      });
    }

    /* email */
    if (fields.indexOf("email") > -1 && this.getColumnValue(cursor, columnNames.indexOf("mimetype")) == this.MIME_TYPES.email) {
      let email = this.getColumnValue(cursor, columnNames.indexOf(this.DATA_TYPES.email.address));
      contact.email.push({
        type: null,
        address: email ? email.trim() : null,
      });
    }

    return contact;
  };
}
