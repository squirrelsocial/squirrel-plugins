# @squirrel-social/contacts

```javascript
ns plugin add @squirrel-social/contacts
```

## Usage

Based on https://github.com/abhayastudios/nativescript-contacts-lite

## License

Apache License Version 2.0
